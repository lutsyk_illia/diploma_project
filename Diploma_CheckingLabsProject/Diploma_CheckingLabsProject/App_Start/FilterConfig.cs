﻿using System.Web;
using System.Web.Mvc;

namespace Diploma_CheckingLabsProject
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
